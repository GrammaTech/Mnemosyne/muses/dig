(defsystem "dig"
  :name "dig"
  :author "GrammaTech"
  :licence "MIT"
  :description "SEL interface to DIG"
  :depends-on (:dig/dig)
  :class :package-inferred-system
  :in-order-to ((test-op (load-op "dig/test")))
  :perform (test-op (o c) (symbol-call :dig '#:run-batch)))
