# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

all: dig

PACKAGE_NAME = dig

LISP_DEPS = $(wildcard *.lisp)

include .cl-make/cl.mk

.qlfile.external:
	touch $@
