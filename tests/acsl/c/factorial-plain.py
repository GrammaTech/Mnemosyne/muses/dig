#!/usr/bin/python3
import sys

# 2 -> 2
# 3 -> 6
# 4 -> 24
def main(argv):
  argc = len(argv)
  i = int(argv[1])
  f = 1

  while (i):
      f *= i
      i -= 1

  print("%ld\n" % f)


if __name__ == "__main__":
    main(sys.argv)
