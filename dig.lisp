;;;; SEL interface to DIG dynamic invariant generation
(uiop/package:define-package :dig/dig
    (:nicknames :dig)
  (:use :gt/full
        :software-evolution-library
        :software-evolution-library/software/compilable
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/python
        :software-evolution-library/software/c
        :software-evolution-library/command-line
        :software-evolution-library/utility/task
        :stefil+
        :cmd)
  (:local-nicknames (:attrs :functional-trees/attrs))
  (:shadow :test)
  (:import-from :infix-math)
  (:export :*dig-command*
           :inject-instrumentation
           :inject-instrumentation-at
           :inject-assertion-at
           :dig
           :infer-and-inject-assertion
           :infer-and-inject-assertion-at
           ;; Test suite
           :test :batch-test))
(in-package :dig)
(in-readtable :curry-compose-reader-macros)

(defparameter *dig-command*
  ;; To run a local dig using the default path inside of the docker image.
  (if (file-exists-p "/dig/src/dig.py")
      "bash -c 'sage -python -O /dig/src/dig.py -timeout 30 <(cat <<EOF
~A
EOF
)'"
      "docker run --rm mnemo/dig bash -c \"cat <<EOF >/tmp/traces.tcs
~A
EOF
sage -python -O dig.py -timeout 30 /tmp/traces.tcs\"")
  "Command used to invoke DIG to generate invariants for a trace.")

(defparameter *assertion-cutoff* 2
  "How many assertiosn to present to the user.")

(defgeneric infer-and-inject-assertion (software tests)
  (:documentation "Infer and inject assertions into SOFTWARE against TEST.")
  (:method ((software tree-sitter) tests)
    ;; Software-wide implementation will require correlating multiple
    ;; AST locations with multiple traces.  This is something that
    ;; inject-instrumentation should hand back as a secondary value.
    (error "TODO: implement `infer-and-inject-assertion'.")))

(defgeneric collect-traces (software tests)
  (:documentation "Collect traces from SOFTWARE (assumed to be insstrumented) over TESTS.")
  (:method ((software tree-sitter) tests)
    (let* ((phenome (phenome software))
           (codes.traces
             ;; Gather traces in parallel.
             (task-map
              (count-cpus)
              (lambda (args)
                (let* (exit-code
                       (stderr
                         (with-output-to-string (s)
                           (setq exit-code
                                 (cmd :ignore-error-status t
                                      (command-prefix software)
                                      phenome
                                      (mapcar #'princ-to-string args)
                                      :> nil
                                      :2> s)))))
                  (cons exit-code (lines stderr))))
              tests))
           (exit-codes (mapcar #'car codes.traces))
           (traces (mapcar #'cdr codes.traces)))
      (unless codes.traces
        (error "No trace output for DIG"))
      ;; Drop header from all but the first trace.
      (values (string-join (apply #'concatenate 'list
                                  (car traces)
                                  (mapcar #'cdr (cdr traces)))
                           (string #\Newline))
              exit-codes))))

(defun verify-software (software tests)
  "Verify SOFTWARE with TESTS.
SOFTWARE should already have assertions added."
  (let ((exit-codes (nth-value 1 (collect-traces software tests))))
    (values (every #'zerop exit-codes)
            exit-codes)))

(defun partition-valid-assertions (software location assertions tests)
  "Given a software object, an AST (location), assertions, and tests,
return two values: a list of valid assertions and a list of invalid
assertions."
  (partition (lambda (assertion)
               (let ((software
                       (inject-assertion-at software location assertion)))
                 (verify-software software tests)))
             assertions))

(defparameter *unwanted-keys* '("temp" "tmp")
  "Heuristic list of variable names we probably don't care about.")

(defgeneric postprocess-traces (software trace)
  (:documentation "Convert TRACE into a format acceptable to DIG.")
  (:method ((software tree-sitter) (trace string))
    trace)
  (:method ((software python) (trace string))
    (flet ((project (alist keys)
             "Do a projection (in the relational-algebra sense) by
keeping only a subset of keys."
             (remove-if-not (op (member (car _) keys :test #'equal))
                            alist))
           (type-abbr (type)
             (ecase type
               (string "S")             ;Supported?
               (float "F")
               (integer "I")
               (character "C")))
           (aget* (key alist)
             (aget key alist :test #'equal)))
      (let* ((lines
               (or (filter {string^= "vtrace"} (lines trace))
                   ;; Give a helpful error instead of the opaque one
                   ;; we would get later from `reduce'.
                   (error "No trace output for DIG")))
             (alists
              (nest
               (mapcar #'cdr)
               (mapcar #'jsown:parse)
               (mapcar (op (drop 1 (drop-until (eqls #\:) _1))))
               (rest)
               lines))
             (common-keys
              (reduce (op (intersection _ _ :test #'equal))
                      (or alists
                          (error "Not enough trace output for DIG"))
                      :key {mapcar #'car}))
             (wanted-keys
              (set-difference common-keys *unwanted-keys* :test #'equal))
             (key-types
              (mapcar (lambda (key)
                        (cons key
                              (let ((values (mapcar {aget* key} alists)))
                                (cond
                                  ((every #'integerp values) 'integer)
                                  ((every #'floatp values) 'float)
                                  ((every #'stringp values)
                                   (cond ((every (of-length 1) values)
                                          'character)
                                         ((some (equals "INVALID") values)
                                          nil)
                                         (t 'string)))
                                  (t nil)))))
                      wanted-keys))
             (valid-keys
              (mapcar #'car (remove-if #'null key-types :key #'cdr)))
             (final-alists
              (mapcar {project _ valid-keys} alists)))
        ;; TODO Is it worth preserving all the error output?
        (with-output-to-string (s)
          (format s "~a ~{~{~a ~a~}~^, ~}~%"
                  (take-until #'whitespacep (first lines))
                  (mapcar (lambda (key)
                            (list (type-abbr (aget* key key-types))
                                  key))
                          valid-keys))
          (iter (for line in (rest lines))
                (for alist in final-alists)
                (format s "~a ~{~a~^, ~}~%"
                        (take-until #'whitespacep line)
                        (mapcar #'cdr alist))))))))

(defgeneric infer-and-inject-assertion-at (software tests ast &key)
  (:documentation "Infer and inject assertions into SOFTWARE at AST against TEST.")
  (:method ((software tree-sitter) tests (ast ast)
            &key (before-trace (constantly nil))
              (before-dig (constantly nil))
              (before-check (constantly nil))
              (verify t))
    (declare (type function before-trace before-dig before-check))
    (let* ((instrumented-software
             (inject-instrumentation-at (copy software) ast))
           (raw-traces
             (progn (funcall before-trace)
                    (collect-traces instrumented-software tests)))
           (traces (postprocess-traces software raw-traces))
           (assertions
             (progn (funcall before-dig)
                    (take *assertion-cutoff*
                          (dig traces))))
           (software
             (inject-assertions-at software ast assertions)))
      (funcall before-check)
      (if (or (null verify) (verify-software software tests))
          software
          (error "Invalid assertions")))))


;;;; Instrumentation:
;;;
;;; Trace format:
;;;     var1 var2...
;;;     val1 val2...
;;;
;;; Print a trace with:
;;;     sage -python -O dig.py ../path/to/trace.tcs -log 3

(defvar *trace-counter*)

(defun vtrace-for-vars (software vars)
  "Build a vtrace printf statement for VARS."
  (incf *trace-counter*)
  (nest
   (attrs:with-attr-table software)
   (let* ((vars (remove-if (of-type '(or function-ast python-import-statement
                                      c-function-declarator))
                           vars :key {aget :decl}))
          (type-and-name
           (remove-if [{eql :pointer} #'first]
                      (filter-map (lambda (var)
                                    (let ((decl (aget :decl var)))
                                      (list
                                       (when-let (type
                                                  (infer-type decl))
                                         (if (typep type 'c-pointer-declarator)
                                             :pointer
                                             (make-keyword
                                              (string-upcase
                                               (source-text type)))))
                                       (aget :name var))))
                                  vars)))))
   (children) (genome)
   (from-string (make-instance (type-of software)))
   (etypecase software
     (c
      (nest
       (apply #'format nil
              "
{
  if(vtrace~d~:*_first_run) {
    vtrace~d~:*_first_run = 0;
    fprintf(stderr, \"vtrace~d: ~{~{~a~^ ~}~^, ~}\\n\");
  }
  fprintf(stderr, \"vtrace~d: ~{~A~^, ~}\\n\", ~{~A~^, ~});
}
"
              *trace-counter*
              (mapcar «list [‹ecase (:float "F") (:int "I") (:long "I") (:char "C") ((:str :string nil) "S")› #'first]
                            #'second»
                      type-and-name)
              *trace-counter*)
       ;; Convert (:int var) to ("%d" var) etc.
       (apply #'mapcar #'list
              (mapcar «list [‹ecase (:float "%f") (:int "%d") (:long "%ld") (:char "%c") ((:str :string nil) "%s")› #'first]
                            #'second»
                      type-and-name))))
     (python
      (format nil
              ;; Emit the traces as JSON. This lets us easily parse
              ;; them and pass on only the ones with values that DIG
              ;; can actually make use of. TODO: There should be some
              ;; logic to keep large values (e.g. strings) from being
              ;; printed.
              "
global vtrace~d~:*_first_run
if vtrace~d~:*_first_run:
    vtrace~d~:*_first_run = 0
    print(\"vtrace~d: ~{~{~a~^ ~}~^, ~}\", file=sys.stderr)
print(\"vtrace~d: \" + json.dumps({~{\"~a\": ~:*~a~^, ~}},
                                  default=lambda o: \"INVALID\"),
      file=sys.stderr)
~%"
              *trace-counter*
              (mapcar (op (list "S" (second _))) type-and-name)
              *trace-counter*
              (mapcar #'second type-and-name))))))

(defun make-c-assert-header ()
  (assure c-preproc-include
    (find-if (of-type 'c-preproc-include)
             (c (fmt "#include <assert.h>~%")))))

(defun c-assert-header? (x)
  (match x
    ((c-preproc-include
      :c-path (c-system-lib-string
               :text "<assert.h>"))
     t)))

(deftype preproc ()
  '(or c-preproc-include python-import-statement))

(defgeneric inject-instrumentation-at (software ast &key &allow-other-keys)
  (:documentation "Inject instrumentation into SOFTWARE at AST to collect traces for DIG.")
  (:method :around (software ast &key &allow-other-keys)
    (let ((*trace-counter* (if (and (boundp '*trace-counter*) (numberp *trace-counter*))
                               *trace-counter*
                               0)))
      (call-next-method)))
  (:method ((software tree-sitter) (while-ast while-ast) &key &allow-other-keys)
    ;; This unintuitive enclosing WITHF call is needed to change the original.
    ;; Add the global declaration of the first_run variable.
    (let* ((software
             (with software
                   (ast-path software (body while-ast))
                   (copy (body while-ast)
                         :children
                         (append (vtrace-for-vars
                                  software (get-vars-in-scope software while-ast))
                                 (slot-value (body while-ast) 'children)))))

           (import-asts
             (econd
               ((typep software 'python)
                (assure python-import-statement
                  (python (fmt "~%import json, sys~%"))))
               ((typep software 'c)
                (make-c-assert-header))))
           (global-var
             (etypecase software
               (c (c (fmt "int vtrace~d_first_run = 1;~%" *trace-counter*)))
               (python
                (python (fmt "vtrace~d_first_run = 1~%" *trace-counter*))))))
      (copy software
            :genome
            (patch-whitespace
             (copy (genome software)
                   :direct-children
                   (append
                    (take-while (of-type 'preproc)
                                (direct-children (genome software)))
                    (list import-asts)
                    (list global-var)
                    (drop-while (of-type 'preproc)
                                (direct-children (genome software))))))))))

(defgeneric inject-instrumentation (software &key &allow-other-keys)
  (:documentation "Inject instrumentation into SOFTWARE to collect traces for DIG.")
  (:method :around (software &key &allow-other-keys)
    (let ((*trace-counter* 0)) (call-next-method)))
  (:method ((software tree-sitter) &key &allow-other-keys)
    (reduce #'inject-instrumentation-at
            (collect-if {typep _ 'while-ast} (genome software))
            :initial-value software)))

(infix-math:declare-binary-operator == :from =)
(infix-math:declare-binary-operator <= :from =)
(infix-math:declare-binary-operator >= :from =)
(setf (infix-math::precedence '==) 10)
(setf (infix-math::precedence '<=) 10)
(setf (infix-math::precedence '>=) 10)

(defun process-formula (string)
  "Process a formula from a string into a sexp."
  (macroexpand
   (let ((*readtable* (copy-readtable nil))
         ;; Needed so we get the right precedence for == etc.
         (*package* (find-package :dig/dig)))
     (setf (readtable-case *readtable*) :preserve)
     (read-from-string
      (string-join (list "(INFIX-MATH:$"
                         ;; Infix-math can parse some, but not all,
                         ;; operators from within symbols. Since DIG
                         ;; isn't using whitespace for precedence we
                         ;; just introduce extra whitespace.
                         (string-join (runs string :key #'alphanumericp)
                                      " ")
                         ")")
                   " ")))))

(defun process-dig-output (raw)
  (mapcar (lambda (inv-line)
            (let ((start (search ". " inv-line)))
              (process-formula (subseq inv-line (+ 2 start)))))
          (cdr (drop-until {search "invs)"}
                           (split-sequence #\Newline raw :remove-empty-subseqs t)))))

(defgeneric dig (trace)
  (:documentation "Run DIG over TRACE returning invariants.")
  (:method ((string string))
    (multiple-value-bind (stdout stderr errno)
        (shell *dig-command* string)
      (assert (zerop errno) (string)
              "DIG against ~S failed with ~S~%~A~%"
              string errno stderr)
      stdout))
  (:method ((pathname pathname))
    (dig (file-to-string pathname)))
  (:method :around (trace) (process-dig-output (call-next-method))))


;;;; Insert assertion \/ Assert insertion
(defgeneric expression-to (software expression)
  (:documentation "Format EXPRESSION as source code.")
  ;; TODO Rewrite to use lenses?
  (:method :around ((software python) expression)
    ;; Python uses `**' for exponentiation, not `^'.
    (string-replace-all "^" (call-next-method) "**"))
  (:method ((software tree-sitter) expression)
    (etypecase expression
      ((cons (eql let) t)
       ;; Inline subexpressions that infix-math has extracted.
       (expression-to software
                      (sublis (mapply #'cons (second expression))
                              (third expression))))
      (cons
       (assert (= 3 (length expression)))
       (format nil "(~a ~a ~a)"
               (expression-to software (second expression))
               (symbol-name (car expression))
               (expression-to software (third expression))))
      (number (format nil "~a" expression))
      (symbol (symbol-name expression)))))

(defvar *c-assertion-format*
  (formatter "assert(~A);")
  ;; "if(!~A~:*){ fprintf(stderr, \"VIOLATED: ~A\\n\"); }~%"
  "Format string used to build an assertion from an invariant.")

(defvar *python-assertion-format*
  (formatter "assert ~A")
  ;; "if(!~A~:*){ fprintf(stderr, \"VIOLATED: ~A\\n\"); }~%"
  "Format string used to build an assertion from an invariant.")

(defun string->assertion-ast (software string)
  (@ (from-string (make-instance (class-of software))
                  (format nil
                          (etypecase software
                            (c *c-assertion-format*)
                            (python *python-assertion-format*))
                          (expression-to software string)))
     0))

(defgeneric inject-assertions-at (software location assertions)
  (:documentation "Insert ASSERTIONS into SOFTWARE at LOCATION.")
  (:method :around ((software c) ast assertions)
    (if (find-if #'c-assert-header? (children (genome software)))
        (call-next-method)
        (let* ((software (call-next-method))
               (g (genome software)))
          (with software nil
                (patch-whitespace
                 (copy g :direct-children
                       (cons (make-c-assert-header)
                             (direct-children g))))))))
  (:method ((software tree-sitter) (while-ast while-ast) (assertions list))
    (let ((assertions
            (mapcar (op (string->assertion-ast software _))
                    assertions))
          (body (body while-ast)))
      ;; TODO Should this be the default behavior of `with' on
      ;; software objects?
      (copy software
            :genome
            (patch-whitespace
             (with (genome software)
                   (ast-path software body)
                   (copy body
                         :direct-children
                         (append assertions
                                 (direct-children body)))))))))

(defgeneric inject-assertion-at (software location assertion)
  (:documentation "Insert ASSERTION into SOFTWARE at LOCATION.")
  (:method ((software tree-sitter) (while-ast while-ast) (assertion list))
    (inject-assertions-at software while-ast (list assertion))))

(defun command-prefix (software)
  (if (typep software 'python)
      (if (resolve-executable "python3")
          "python3 "
          "python ")
      nil))
