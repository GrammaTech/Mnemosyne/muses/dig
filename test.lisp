(defpackage :dig/test
  (:use)
  (:import-from :dig/dig))
(in-package :dig)                       ;!
(in-readtable :curry-compose-reader-macros)

(defvar *soft* nil "Holds software object for tests.")

(defroot test)

(defsuite dig-test "Dig tests")

(defixture example.c
  (:setup (setf *soft* (from-file (make-instance 'c :compiler "gcc")
                                  (asdf:system-relative-pathname
                                   :dig "tests/acsl/c/factorial-plain.c"))))
  (:teardown (setf *soft* nil)))

(defixture example.py
  (:setup (setf *soft* (from-file (make-instance 'python)
                                  (asdf:system-relative-pathname
                                   :dig "tests/acsl/c/factorial-plain.py"))))
  (:teardown (setf *soft* nil)))

;;; TODO Add to stefil+?
(defmacro with-each-fixture ((&rest fixtures) &body body)
  "Run BODY with multiple fixtures."
  (with-thunk (body)
    `(progn                             ;XXX
       ,@(iter (for fixture in fixtures)
               (collect `(with-fixture ,fixture (,body)))))))

(deftest inject-instrumentation-simple-example ()
  ;; Confirm that we can load and inject trace instrumentation into an example C program,
  (with-each-fixture (example.c example.py)
    (setf *soft* (inject-instrumentation *soft*))
    ;; and then find the expected instrumentation fprintf,
    (is (find-if «and {typep _ 'identifier-ast}
                      [{string^= "print"} #'source-text]»
                 *soft*))
    ;; and then compiles successfully.
    (is (zerop (nth-value 1 (phenome *soft*))))))

(deftest inject-instrumentation-and-run-dig ()
  (with-each-fixture (example.c example.py)
    (multiple-value-bind (stdout stderr errno)
        (shell (concatenate 'string
                            (command-prefix *soft*)
                            (phenome (inject-instrumentation *soft*))
                            " 8"))
      (declare (ignorable stdout))
      (is (zerop errno))
      (let ((invariants (dig (postprocess-traces *soft* stderr))))
        (is (not (null invariants)))
        (is (every #'listp invariants))
        (is (every [{member _ '(== <= >=)} #'first] invariants))
        invariants))))

(deftest insert-invariant-assertions ()
  (with-each-fixture (example.c example.py)
    (let ((invariants '((== (- |argc| 2) 0)
                        (<= (* -1 |i|) -1)
                        (<= (- (* -1 |f|) |i|) -9)
                        (<= (+ |argc| |i|) 10)))
          (*c-assertion-format* "if(!~A~:*){ fprintf(stderr, \"VIOLATED: ~A\\n\"); }~%")
          (*python-assertion-format* "if not ~A~:*: print(\"VIOLATED: ~A\\n\", file=sys.stderr)~%"))
      (setf *soft*
            (inject-assertion-at *soft* (find-if {typep _ 'while-ast} *soft*) (first invariants)))
      (is (search "argc - 2" (genome-string *soft*)))
      (multiple-value-bind (stdout stderr errno)
          (shell (concatenate 'string
                              (command-prefix *soft*)
                              (phenome (inject-instrumentation *soft*))
                              " 8"))
        (declare (ignorable stdout))
        (is (zerop errno))
        (is (not (some {search "VIOLATED"} (split-sequence #\Newline stderr))))))))

(deftest insert-end-to-end-invariant-generation-and-assertions ()
  (with-each-fixture (example.c example.py)
    (let ((invariants '((== (- |argc| 2) 0)
                        (<= (* -1 |i|) -1)
                        (<= (- (* -1 |f|) |i|) -9)
                        (<= (+ |argc| |i|) 10)))
          (*c-assertion-format* "if(!~A~:*){ fprintf(stderr, \"VIOLATED: ~A\\n\"); }~%")
          (*python-assertion-format* "if not ~A~:*: print(\"VIOLATED: ~A\\n\", file=sys.stderr)~%"))
      (setf *soft*
            (inject-assertion-at *soft* (find-if {typep _ 'while-ast} *soft*) (first invariants)))
      (is (search "argc - 2" (genome-string *soft*)))
      (multiple-value-bind (stdout stderr errno)
          (shell (concatenate 'string
                              (command-prefix *soft*)
                              (phenome (inject-instrumentation *soft*))
                              " 8"))
        (declare (ignorable stdout))
        (is (zerop errno))
        (is (not (some {search "VIOLATED"} (split-sequence #\Newline stderr))))))))

(deftest test-infer-and-inject-assertion-at ()
  (with-each-fixture (example.c example.py)
    (infer-and-inject-assertion-at
     *soft* '(("1") ("2") ("3") ("4"))
     (find-if {typep _ 'while-ast} *soft*)
     :verify nil)))

(deftest test-process-formula-package ()
  (let ((*package* (find-package :cl-user)))
    (is (equal '== (first (process-formula "k*x - i*y + B == 0"))))))

(deftest test-exponentiation-operator-python ()
  (let ((expr (process-formula "A^2*m - A^2")))
    (is (search "^" (expression-to (make 'c) expr)))
    (is (not (search "^" (expression-to (make 'python) expr))))
    (is (search "**" (expression-to (make 'python) expr)))))

(deftest test-formula-parsing ()
  (let ((f "a^2*m - a^2 - 624166*a*m + 624166*a + 23092773*m - 23092773 == 0"))
    (is (not (find-if (of-type 'parse-error-ast)
                      (string->assertion-ast (make 'python)
                                             (process-formula f)))))))
